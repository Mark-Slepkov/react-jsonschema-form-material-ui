import React, {Component} from 'react';
import logo from './logo.png';
import './App.css';
import QuantityInput from './ui/QuantityInput'
import Input from './ui/Input'
import SelectSingle from './ui/Select/Single'
import SelectMultiple from './ui/Select/Multiple'
import SelectChipMultiple from './ui/Select/ChipMultiple'
import ErrorBox from './ui/ErrorBox'
import Preloader from './ui/Preloader'
import {MuiThemeProvider} from '@material-ui/core/styles';
import Warning from './ui/Warning';
import theme from './theme'
import ImagePicker from './ui/ImagePicker';
import Slider from "./ui/Slider";
import ColorPicker from "./ui/ColorPickerRestricted";
import Checkbox from "./ui/Checkbox";
// import './ui/quantity_input/styleSchema.sass'

class App extends Component {
    state = {
        value: 1,
    };

    render() {

        const selectMultipleSchema = {
            type: 'array',
            items: {
                type: 'string',
                enum: ['x1', 'x2', 'x3'],
                enumNames: ['X one', 'X two', 'X three']
            }
        };

        const chipSchema = {
            type: 'array',
            items: {
                type: 'string',
                enum: ['x1', 'x2', 'x3'],
                enumNames: ['X one', 'X two', 'X three']
            }
        };
        return (
            <MuiThemeProvider theme={theme}>
                <div className="App">
                    <header className="App-header">

                        <img src={logo} className="App-logo" alt="logo"/>
                        <h1 className="App-title">Welcome to react-jsonschema-material-ui</h1>
                    </header>
                    <QuantityInput
                        value={this.state.value}
                        onChange={(value) => {
                            this.setState({value})
                        }}
                    />
                    <br/>
                    <Checkbox options={{title: 'Checkbox'}} value={true} />
                    <hr/>
                    <Preloader state={'loading'} onReload={() => alert('Reload')}/>
                    <hr/>
                    <Preloader state={'error'}/>
                    <br/>
                    <Input placeholder="Input text..."/>
                    <br/>
                    <SelectSingle/>
                    <br/>
                    <SelectMultiple schema={selectMultipleSchema} value={[]}/>
                    <br/>
                    <SelectChipMultiple schema={chipSchema} options={{enumOptions: [{value: 'a1', label: 'A one'}]}}/>
                    <br/>
                    <Slider min={10} max={100} />

                    <br/>
                    <ColorPicker colors={['#000000', '#aabbcc', '#00aa11', '#aa0033']} />
                    <br/>
                    <Warning visible={true}>Warning text</Warning>
                    <br/>
                    <ErrorBox>{['Error1', 'Error2']}</ErrorBox>
                    <br/>
                    <ErrorBox align={'left'}>{['Left aligned Error1', 'Left aligned Error2']}</ErrorBox>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;
