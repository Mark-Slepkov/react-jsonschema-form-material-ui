export const makeCols = (theme) => {
    return (xsColSize: number, smColSize: number, mdColSize: number, lgColSize: number, xlColSize: number): any => {
        return {
            [theme.breakpoints.only('xs')]: {
                width: `${((xsColSize * 100) / 12)}%`
            },
            [theme.breakpoints.only('sm')]: {
                width: `${((smColSize * 100) / 12)}%`
            },
            [theme.breakpoints.only('md')]: {
                width: `${((mdColSize * 100) / 12)}%`
            },
            [theme.breakpoints.only('lg')]: {
                width: `${((lgColSize * 100) / 12)}%`
            },
            [theme.breakpoints.only('xl')]: {
                width: `${((xlColSize * 100) / 12)}%`
            },
        }
    };
};