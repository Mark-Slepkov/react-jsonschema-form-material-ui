import {createStyles} from '@material-ui/styles';
import {makeCols} from "../../utils";

const styleSchema = (theme) => {
    return createStyles({
        input1: {
            padding: '0 10px',
            boxSizing: 'border-box',
            ...makeCols(theme)(12, 12, 6, 3, 3)
        },
        input2: {
            ...makeCols(theme)(12, 12, 6, 3, 3)
        },
        select1: {
            ...makeCols(theme)(12, 12, 6, 3, 3)
        },
        radio1: {
            ...makeCols(theme)(12, 12, 6, 3, 3)
        },
    });
};
export default styleSchema;
