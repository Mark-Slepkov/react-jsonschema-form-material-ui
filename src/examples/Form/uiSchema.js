import Input from '../../ui/Input'
import Select from '../../ui/Select/Single'
import RadioGroupPicker from "../../ui/RadioGroupPicker";
import ObjectFieldTemplateRow from "../../ui/Form/ObjectFieldTemplate/Row";
import ImagePicker from "../../ui/ImagePicker";
import Switch from '../../ui/Switch';

export default () => {

    return {
        'ui:ObjectFieldTemplate': ObjectFieldTemplateRow,
        input1: {
            'ui:widget': Input,
            'ui:title': 'Input №1',
            'ui:placeholder': 'Placeholder Input 1',
        },
        input2: {
            'ui:widget': Input,
            'ui:title': 'Input №2',
            'ui:placeholder': 'Placeholder Input 2',
        },
        description: {
            'ui:title': 'Описание',
            'ui:widget': Input,
            'ui:rowsMax': 4,
            'ui:rows': 2,
            'ui:multiline': true
        },
        select1: {
            'ui:widget': Select,
            'ui:title': 'Select №1',
            'ui:placeholder': 'Placeholder 1',
        },
        radio1: {
            'ui:title': 'Radio №1',
            'ui:widget': RadioGroupPicker,
        },
        avatar: {
            'ui:title': 'Load Photo',
            'ui:rounded': true,
            'ui:widget': ImagePicker,
            'ui:imageMaxWidth': 500,
        },
        switch1: {
            'ui:title': 'Switch One',
            'ui:widget': Switch
        }
    }
}
