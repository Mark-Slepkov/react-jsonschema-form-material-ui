export default {
    type: 'object',
    properties: {
        input1: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        input2: {
            type: 'number'
        },
        select1: {
            type: 'string',
            enum: ['1', '2', '3'],
            enumNames: ['Item 1', 'Item 2', 'Item 3'],
        },
        radio1: {
            type: 'string',
            enum: ['1', '2', '3'],
            enumNames: ['Item 1', 'Item 2', 'Item 3'],
        },
        avatar: {
            type: 'string',
        },
        switch1: {
            type: 'boolean'
        }
    },
    required: ['input2', 'select1']
}
