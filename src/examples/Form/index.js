import React from 'react'
import Form from '../../ui/Form'
import schema from './schema'
import uiSchema from './uiSchema'
import styleSchema from './styleSchema';
import {makeStyles} from "@material-ui/core/styles";

const makeClasses = makeStyles(styleSchema);

class StyledTestForm extends React.Component {

    state = {
        form: {}
    };

    render() {
        return <div>
            FORM: 1
            <Form
                schema={schema}
                uiSchema={uiSchema()}
                styleSchema={styleSchema}
                formData={this.state.form}
                classes={this.props.formClasses}
                onChange={({formData}) => this.setState({form: formData})}
            />
        </div>
    }

}

const TestForm = (props) => {
    const formClasses = makeClasses(props);
    return <StyledTestForm {...props} formClasses={formClasses} />
};


export default TestForm