import {createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#532b00", //"rgb(96, 80, 177)"; // sass: lighten($color_font_dark, 25%)
        },
        text: {primary: "#532b00"},
        // error: {
        //     main: '#f03030'
        // },
        typography: {
            button: {
                color: '#ffffff'
            }
        }
    },
    //"rgb(96, 80, 177)"; // sass: lighten($color_font_dark, 25%)
    fontFamily: "fontFamily: \"PT Sans\", sans-serif"
});

export default theme
