import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        error: {
            display: 'flex',
            flexDirection: 'column'
        },
        errorMsg: {
            textAlign: 'center',
            padding: '15px 0'
        },
        errorButtonReload: {
            display: 'flex',
            flexDirection: 'row,',
            justifyContent: 'center',
            alignItems: 'center'
        },
        buttonReload: {
            cursor: 'pointer'
        }
    });
};
export default style;
