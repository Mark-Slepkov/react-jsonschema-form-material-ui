import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
import AutoRenew from '@material-ui/icons/Autorenew';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/core/styles";
import style from './style'

const makeClasses = makeStyles(style);

class StyledPreloader extends React.Component {

    static propTypes = {
        onReload: PropTypes.func,
        errorSilent: PropTypes.bool,
        errorText: PropTypes.string
    };

    static defaultProps = {
        onReload: () => {
        },
        errorSilent: false,
        errorText: "Error while loading. Please check your network connection and repeat."
    };

    render() {
        let content = null;
        const classes = this.props.classes;
        switch (this.props.state) {
            case 'pending':
            case 'loading':
                content = <CircularProgress/>;
                break;
            case 'failed':
            case 'error':
                if (!this.props.errorSilent) {
                    content = (
                        <div className={classes.error}>
                            <div className={classes.errorMsg}>
                                {this.props.errorText}
                            </div>
                            <div className={classes.errorButtonReload}>
                                <div onClick={this.props.onReload.bind(this)}>
                                    <AutoRenew/>
                                </div>
                            </div>
                        </div>
                    )
                }
        }
        return <div className="preloader">
            {content}
        </div>
    }
}

const Preloader = (props) => {
    const classes = makeClasses(props)
    return <StyledPreloader {...props} classes={classes}/>
}

export default Preloader
