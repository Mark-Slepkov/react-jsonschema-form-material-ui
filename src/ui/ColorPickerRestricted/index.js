import React from 'react'
import Button from '@material-ui/core/Button';
import _isFunction from 'lodash/isFunction'
import style from './style'
import {makeStyles} from "@material-ui/core/styles";

const makeClasses = makeStyles(style)

// TODO: This component is not compatible with react-jsonschema-form. We need to adjust it.

class StyledColorPicker extends React.Component {

    constructor(props) {
        super(props);
        let defaultItem = this.props.default || 0;
        this.state = {
            color: this.props.colors[defaultItem]
        }
    }

    componentWillMount() {
        let color = this.state.color;
        _isFunction(this.props.onChange) && this.props.onChange(color)
    }

    onChange(color) {
        this.setState({color: color});
        _isFunction(this.props.onChange) && this.props.onChange(color)
    }

    render() {
        let colors = this.props.colors;
        const classes = this.props.classes;
        return (
            <div className={classes.layout}>
                <div className={classes.title}>{this.props.title}</div>
                <div className={classes.items}>
                    {colors.map((color, index) => {
                        let active = colors[index] === this.state.color;
                        return (
                            <div className={classes.item} key={index}>
                                <Button
                                    className={`${classes.button} ${active && classes.buttonActive}`}
                                    label=""
                                    primary={true}
                                    onClick={() => this.onChange(color)}
                                    style={{backgroundColor: color}}
                                />
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

const ColorPicker = (props) => {
    const classes = makeClasses(props)
    return <StyledColorPicker {...props} classes={classes} />
}

export default ColorPicker
