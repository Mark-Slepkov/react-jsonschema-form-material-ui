import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        layout: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'flex-start',
            height: '72px'
        },
        items: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            height: '30px'
        },
        item: {
            padding: '0 2px'
        },
        button: {
            boxShadow: '1px 1px 3px 0 rgba(0, 0, 0, 0.3)',
            width: '25px !important',
            minWidth: '25px !important',
            height: '25px !important',
            borderRadius: '100px !important',
        },
        buttonActive: {
            width: '30px !important',
            minWidth: '30px !important',
            height: '30px !important',
            boxShadow: '1px 1px 3px 0 rgba(0, 0, 0, 0.5)'
        },
        title: {
            color: '#666666',
            fontSize: '10pt'
        }
    });
};
export default style;
