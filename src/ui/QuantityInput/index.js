import React from 'react'
import {Input, IconButton} from '@material-ui/core'
import IconMinus from '@material-ui/icons/Remove'
import IconPlus from '@material-ui/icons/Add'
import _isNaN from 'lodash/isNaN'
import _isFunction from 'lodash/isFunction'
import _isUndefined from 'lodash/isUndefined'
import PropTypes from 'prop-types'
import {makeStyles} from "@material-ui/core/styles";
import style from './style'

const makeClasses = makeStyles(style);


class StyledQuantityInput extends React.Component {

    static propTypes = {
        onChange: PropTypes.func.isRequired,
        disabled: PropTypes.bool,
        placeholder: PropTypes.string,
        tabIndex: PropTypes.number,
        value: PropTypes.number,
        maxValue: PropTypes.number,
        minValue: PropTypes.number,
    };

    static defaultProps = {
        minValue: 0,
    }

    handlerChange(event) {
        let value = event.target.value;
        let newValue = _isNaN(Number(parseInt(value, 10))) ? this.props.minValue : Number(parseInt(value, 10));
        this.props.onChange(this.correctValue(newValue))
    }

    handlerInc(...args) {
        let value = _isNaN(Number(parseInt(this.props.value, 10))) ? this.props.minValue : Number(parseInt(this.props.value, 10));
        this.props.onChange(this.correctValue(value + 1))
    }

    handlerDec() {
        let value = _isNaN(Number(parseInt(this.props.value, 10))) ? this.props.minValue : Number(parseInt(this.props.value, 10));
        if (_isFunction(this.props.onChange)) {
            if (!_isUndefined(this.props.minValue) && (value > this.props.minValue)) {
                this.props.onChange(this.correctValue(value - 1))
            }
        }
    }

    correctValue(val) {
        let value = val;
        let maxValue = this.props.maxValue;
        let minValue = this.props.minValue;
        // let errors = data.error[this.props.name];
        if (!_isUndefined(maxValue)) {
            if (val > maxValue) {
                value = maxValue
            }
        }
        if (!_isUndefined(minValue)) {
            if (val < minValue) {
                value = minValue
            }
        }
        return value
    }

    render() {
        let value = this.correctValue(this.props.value);
        const classes = this.props.classes;
        return (
            <div className={classes.layout}>
                <div className={classes.label}>
                    <div className={classes.title}>{this.props.title}</div>
                    <div className={classes.inputWrapper}>
                        <IconButton
                            key={1}
                            disabled={this.props.disabled}
                            // classes={this.props.classes}
                            tabIndex={null}
                            onClick={this.handlerDec.bind(this)}>
                            <IconMinus />
                        </IconButton>
                        <Input
                            key={2}
                            classes={classes.input}
                            ref={(input) => {
                                this.currentInput = input
                            }}
                            onChange={(...args)=>this.handlerChange(...args)}
                            type={this.props.type}
                            placeholder={this.props.placeholder}
                            value={value || 0}
                            tabIndex={this.props.tabIndex}
                            disabled={this.props.disabled}
                        />
                        <IconButton
                            key={3}
                            disabled={this.props.disabled}
                            tabIndex={null}
                            onClick={(...args) => this.handlerInc(...args)}>
                            <IconPlus />
                        </IconButton>
                    </div>
                </div>
            </div>
        )
    }
}

const QuantityInput = (props) => {
    const classes = makeClasses(props)
    return <StyledQuantityInput {...props} classes={classes} />
}

export default QuantityInput
