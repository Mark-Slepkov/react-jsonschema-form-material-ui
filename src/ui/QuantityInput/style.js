import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        layout: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            width: '135px'
        },
        title: {},
        label: {},
        inputWrapper: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        button: {
            width: '30px',
            height: '30px',
            borderRadius: '30px'
        },
        input: {
            width: '50px',
        }
    });
};
export default style;
