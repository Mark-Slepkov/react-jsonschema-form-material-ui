import React from 'react';
import PropTypes from 'prop-types';
import IconCamera from '@material-ui/icons/PhotoCamera'
import IconGallery from '@material-ui/icons/Camera'
import IconCancel from '@material-ui/icons/Cancel'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {makeStyles} from "@material-ui/core/styles";
import {fileToBase64} from "../helpers/file";
import Browser from 'browser-detect';
import _ from 'lodash';
import style from './style'

const makeClasses = makeStyles(style);

class StyledImagePickerMenu extends React.Component {

    static propTypes = {
        value: PropTypes.string,
        open: PropTypes.bool.isRequired,
        onChange: PropTypes.func.isRequired,
        onTakePhoto: PropTypes.func.isRequired,
        onClose: PropTypes.func.isRequired
    };

    onSelect = (value) => {
        this.props.onChange(value);
        this.props.onClose();
    };

    onClose = () => {
        this.props.onClose()
    };

    onFileInputChange = async (e) => {
        const file = e.target.files[0];
        const base64 = await fileToBase64(file);
        this.props.onChange('gallery');
        this.props.onTakePhoto(base64);
        this.props.onClose()
    };

    render() {
        const browser = Browser();
        return <Menu open={this.props.open} anchorEl={this.props.anchorEl}
                     onClose={() => this.onSelect(this.props.value)}>
            <MenuItem className={this.props.classes.galleryMenuItem}>
                <IconGallery/>&nbsp;Gallery
                <input type="file" onChange={this.onFileInputChange} className={this.props.classes.fileInput}/>
            </MenuItem>
            {!browser.os.match(/os x/i) &&
            <MenuItem onClick={() => this.onSelect('camera')}>
                <IconCamera/>&nbsp;Camera
            </MenuItem>
            }
            <MenuItem onClick={this.onClose}>
                <IconCancel/>&nbsp;Cancel
            </MenuItem>
        </Menu>
    }

}

const ImagePickerMenu = (props) => {
    const classes = makeClasses(props);
    return <StyledImagePickerMenu {...props} classes={classes}/>
};

export default ImagePickerMenu
