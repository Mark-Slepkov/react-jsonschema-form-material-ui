import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        layout: {

        },
        galleryMenuItem: {
            position: 'relative'
        },
        fileInput: {
            position: 'absolute',
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            opacity: 0
        },
    });
};
export default style;
