export const byte64ToBlob = (b64Data, content_type, slice_size) => {
    content_type = content_type || '';
    slice_size = slice_size || 512;

    const byte_characters = atob(b64Data);
    const byte_arrays = [];

    for (let offset = 0; offset < byte_characters.length; offset += slice_size) {
        const slice = byte_characters.slice(offset, offset + slice_size);
        const byte_numbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byte_numbers[i] = slice.charCodeAt(i);
        }
        const byte_array = new Uint8Array(byte_numbers);
        byte_arrays.push(byte_array);
    }
    const blob = new Blob(byte_arrays, {type: content_type});
    return blob;
};

export const base64ToBlob = (image_url) => {
    // Split the base64 string in data and content_type
    const block = image_url.split(";");
    // Get the content type of the image
    const content_type = block[0].split(":")[1];// In this case "image/gif"
    // get the real base64 content of the file
    const data = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
    // Convert it to a blob to upload
    return byte64ToBlob(data, content_type);
};

export const fileToBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
};

export const randomNameFile = (len = 5, alphabet = "abcdefghijklmnopqrstuvwxyz") => {
    let text = "";
    alphabet = typeof(alphabet)==='string'?alphabet:'abcdefghijklmnopqrstuvwxyz'
    for( let i=0; i < len; i++ )
        text += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
    return text;
}
