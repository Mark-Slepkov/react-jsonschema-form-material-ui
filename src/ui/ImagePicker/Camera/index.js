import React from 'react'
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {makeStyles} from "@material-ui/core/styles";
import IconFlipCameraIos from "@material-ui/icons/FlipCameraIos";
import IconFiberManualRecord from "@material-ui/icons/FiberManualRecord";
import Webcam from "react-webcam";
import style from './style'

const makeClasses = makeStyles(style);

class StyledCameraPicker extends React.Component {

    static propTypes = {
        onClose: PropTypes.func.isRequired,
    };

    state = {
        facingMode: 'user',
    };

    onTakePhoto = (dataUri) => {
        console.log(dataUri);
        this.props.onTakePhoto(dataUri);
        this.props.onClose();
    };

    takePhoto = () => {
        const dataUri = this.webCam.getScreenshot();
        this.props.onTakePhoto(dataUri);
        this.props.onClose();
        this.webCam.getScreenshot();
    };

    toggleFacingMode = () => {
        if (this.state.facingMode === 'user') {
            this.setState({facingMode: 'environment'})
        } else {
            this.setState({facingMode: 'user'})
        }
    };

    render() {
        return <Dialog open={true} onClose={this.props.onClose} className={this.props.classes.layout}>
            <div className={this.props.classes.buttonCloseContainer}>
                <IconButton onClick={this.props.onClose}>
                    <CloseIcon/>
                </IconButton>
            </div>
            <div className={this.props.classes.cameraContainer}>
                <Webcam
                    // Camera does not disable after flip. This property makes it fixed.
                    key={this.state.facingMode}
                    ref={(ref) => this.webCam = ref}
                    videoConstraints={{facingMode: this.state.facingMode}}
                    mirrored={this.state.facingMode === 'user'}
                    screenshotFormat="image/jpg"
                    audio={false}
                />
                <div className={this.props.classes.controls}>
                    <IconButton onClick={this.takePhoto}><IconFiberManualRecord/></IconButton>
                    <IconButton onClick={this.toggleFacingMode}><IconFlipCameraIos/></IconButton>
                </div>
            </div>
        </Dialog>
    }

}

const CameraPicker = (props) => {
    const classes = makeClasses(props);
    return <StyledCameraPicker {...props} classes={classes}/>
};

export default CameraPicker
