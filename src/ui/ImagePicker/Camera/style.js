import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        layout: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            // position: 'fixed',
            // top: 0,
            // left:0,
            // right: 0,
            // bottom: 0,
            // zIndex: 100000,
            '& video': {
                width: 'auto',
                maxWidth: '100%',
                maxHeight: '100%',
            }
        },
        buttonCloseContainer: {
            position: 'absolute',
            right: 0,
            top: 0,
            zIndex: 1
        },
        cameraContainer: {
            position: 'relative',
            zIndex: 0
        },
        buttonFlipContainer: {
            position: 'absolute',
            right: 0,
            bottom: '50%',
            zIndex: 1
        },
        controls: {
            position: 'absolute',
            left: 0,
            bottom: 0,
            zIndex: 1,
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center'
        }
    });
};
export default style;
