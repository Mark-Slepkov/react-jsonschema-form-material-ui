import {createStyles} from '@material-ui/styles';

const style = (theme, props) => {
    return createStyles({
        layout: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            width: '150px',
            height: '100px',
            // borderColor: theme.palette.text.secondary,
            // borderWidth: '1px',
            // borderStyle: 'solid'
        },
        background: {
            width: '100%',
            height: '100%',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            position: 'relative'
        },
        button: {
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
        },
        title: {
            color: theme.palette.text.secondary,
        },
        buttonDelete: {
            position: 'absolute',
            top: 0,
            right: 0,
            opacity: 0.8
        },
        buttonDeleteDisabled: {
            position: 'absolute',
            top: 0,
            right: 0,
            opacity: 0.8,
            color: theme.palette.text.secondary,
        }
    });
};
export default style;
