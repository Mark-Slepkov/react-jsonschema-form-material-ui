import React from 'react'
import PropTypes from 'prop-types'
import IconCamera from '@material-ui/icons/AddAPhoto'
import IconDelete from '@material-ui/icons/Delete'
import {makeStyles} from "@material-ui/core/styles";
import ButtonBase from "@material-ui/core/ButtonBase";
import Paper from "@material-ui/core/Paper";
import _ from 'lodash';
import Menu from './Menu'
import style from './style'
import Camera from "./Camera";

const makeClasses = makeStyles(style);

class StyledImagePicker extends React.Component {

    static propTypes = {
        classes: PropTypes.shape({
            layout: PropTypes.string.isRequired,
            button: PropTypes.string.isRequired,
            background: PropTypes.string.isRequired,
        }).isRequired,
        value: PropTypes.string,
    };

    state = {
        source: null,
        isMenuOpen: false,
        menuAnchor: null
    };

    openMenu = (event) => {
        this.setState({isMenuOpen: true, menuAnchor: event.currentTarget})
    };

    onChangeSource = (value) => {
        this.setState({
            source: value,
        })
    };

    onMenuClose = () => {
        this.setState({
            isMenuOpen: false
        })
    };

    clear = () => {
        this.props.onChange(undefined)
    };

    onCloseCamera = () => {
        this.setState({source: null})
    };

    render() {
        const classButtonDelete = this.props.disabled ? this.props.classes.buttonDeleteDisabled : this.props.classes.buttonDelete;
        let style = {}
        if (typeof (this.props.value) === 'string') {
            style['backgroundImage'] = `url(${this.props.value})`
        }
        return <Paper className={this.props.classes.layout}>
            <div className={this.props.classes.background} style={style}>
                <Menu
                    value={this.state.source}
                    open={this.state.isMenuOpen}
                    anchorEl={this.state.menuAnchor}
                    onChange={this.onChangeSource}
                    onTakePhoto={this.props.onChange}
                    onClose={this.onMenuClose}
                />
                {this.props.value ?
                    <ButtonBase className={classButtonDelete} onClick={this.clear} disabled={this.props.disabled}>
                        <IconDelete/>
                    </ButtonBase>
                    :
                    <ButtonBase className={this.props.classes.button} onClick={this.openMenu}
                                disabled={this.props.disabled}>
                        <div className={this.props.classes.title}>
                            <IconCamera/>
                            <div>{_.get(this.props.options, 'title', this.props.title)}</div>
                        </div>
                    </ButtonBase>
                }
                {(this.state.source === 'camera') && <Camera
                    onClose={this.onCloseCamera}
                    onTakePhoto={this.props.onChange}
                />}
            </div>
        </Paper>
    }

}

const ImagePicker = (props) => {
    const classes = makeClasses(props);
    return <StyledImagePicker {...props} classes={classes}/>
};

export default ImagePicker
