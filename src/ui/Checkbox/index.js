import React from 'react'
import MUICheckbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import style from './style'
import {makeStyles} from "@material-ui/core/styles";


const makeClasses = makeStyles(style)

class StyledCheckbox extends React.Component {


    onInput() {
        this.props.onChange(!this.props.value)
    }


    render() {
        let value = this.props.value;
        return (
            <FormControlLabel
                label={this.props.options.title}
                control={<MUICheckbox
                    color="primary"
                    onChange={() => this.onInput()}
                    checked={value || false}
                    tabIndex={this.props.tabIndex}
                />}
            />
        )
    }
}

const Checkbox = (props) => {
    const classes = makeClasses(props)
    return <StyledCheckbox {...props} classes={classes} />
}

export default Checkbox
