import React from 'react'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import _get from 'lodash/get';
import _map from 'lodash/map';
import InputLabel from "@material-ui/core/InputLabel";

class RadioGroupPicker extends React.Component {

    onChange(e) {
        console.log('onChange', e);
        this.props.onChange(e.target.value)
    }

    render() {
        const enumNames = _get(this.props.schema, 'enumNames', []);
        const enumIds = _get(this.props.schema, 'enum', []);
        const disabled = this.props.disabled;
        return <FormControl>
            <FormLabel>{_get(this.props.options, 'title', this.props.title)}</FormLabel>
            {/*<InputLabel htmlFor={this.inputId}>{_get(this.props, 'options.title', this.props.title)}</InputLabel>*/}
            <RadioGroup
                value={this.props.value}
                onChange={(e) => this.onChange(e)}
                row={true}
            >
                {_map(enumIds, (item, index) => {
                    const label = enumNames[index] || item;
                    return <FormControlLabel
                        key={index}
                        value={item}
                        control={<Radio color="primary"/>}
                        label={label}
                        disabled={disabled}
                        checked={item === this.props.value}
                    />
                })}
            </RadioGroup>
        </FormControl>
    }

}

export default RadioGroupPicker
