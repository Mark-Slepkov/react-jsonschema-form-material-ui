import React from 'react'
import Slider from '@material-ui/core/Slider'
// import ErrorBox from 'modules/ui/ErrorBox'
import _isUndefined from 'lodash/isUndefined'
import {makeStyles} from '@material-ui/core/styles';
import style from './style'
const makeClasses = makeStyles(style)
// import './style.sass'


class StyledSlider extends React.Component {

    render() {
        let value = this.props.value;
        let errors = this.props.errors;
        const classes = this.props.classes;
        return (
            <div className={classes.layout}>
                <label className={classes.label}>
                    <div className={classes.title}>{this.props.title}</div>
                    <div className={classes.currentValue}>
                        {this.props.formattedValue || value || ''}
                    </div>
                    <Slider min={this.props.min}
                            max={this.props.max}
                            step={this.props.step}
                            onChange={(e, value) => this.props.onChange(e, value)}
                            value={value}
                            {...this.props}
                    />
                    <div className={classes.minMaxWrapper}>
                        <div className={classes.minValue}>
                            {(!_isUndefined(this.props.formattedMin)) ? this.props.formattedMin : this.props.min}
                        </div>
                        <div className={classes.maxValue}>
                            {(!_isUndefined(this.props.formattedMax)) ? this.props.formattedMax : this.props.max}
                        </div>
                    </div>
                    {/*<ErrorBox vocabulary={this.props.vocabulary}>*/}
                    {/*    {errors}*/}
                    {/*</ErrorBox>*/}
                </label>
            </div>
        )
    }
}

export default (props) => {
    const classes = makeClasses(props)
    return <StyledSlider {...props} classes={classes} />
}
