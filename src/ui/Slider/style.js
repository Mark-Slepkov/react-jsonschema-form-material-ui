import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        layout: {},
        label: {
            display: 'flex',
            flexDirection: 'column'
        },
        currentValue: {
            width: '100%',
            //margin-bottom: -25px
            textAlign: 'center',
            height: '2ex'
        },
        minMaxWrapper: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between'
            //margin-top: -50px`
        }
    });
};
export default style;
