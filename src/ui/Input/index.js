import React from 'react'
import TextField from '@material-ui/core/TextField'
import _get from 'lodash/get'
import {makeStyles} from "@material-ui/core/styles";
import style from './style'

const makeClasses = makeStyles(style);


class StyledInput extends React.Component {

    onInput(event) {
        // let value = (this.currentInput.value === "") ? undefined : this.currentInput.value;
        this.props.onChange(event.target.value)
    }

    render() {
        let value = this.props.value;
        return (
            <TextField
                //defaultValue="Default Value"
                //variant="outlined"
                disabled={this.props.disabled}
                label={_get(this.props.options, 'title', this.props.title)}
                type={_get(this.props.options, 'type', 'text')}
                margin="dense"
                className={this.props.classes.input}
                onChange={this.onInput.bind(this)}
                // Be careful! This trick prevents hanging of float labels after clear data
                value={value || ''}
                ///////////////////////////////////////////////////////////////////////////
                placeholder={_get(this.props, 'placeholder', '')}
                multiline={_get(this.props, 'options.multiline', false)}
                rows={_get(this.props, 'options.rows', 0)}
                rowsMax={_get(this.props, 'options.rowsMax', 0)}
                variant={_get(this.props, 'options.variant', undefined)}
                InputProps={_get(this.props, 'options.InputProps')}
            />
        )
    }
}

const Input = (props) => {
    const classes = makeClasses(props);
    return <StyledInput {...props} classes={classes}/>
};

export default Input
