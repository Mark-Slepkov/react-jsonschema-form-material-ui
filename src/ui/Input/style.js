import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        input: {
            width: '100%',
        },
    });
};
export default style;
