import React from 'react'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import {withStyles, makeStyles} from '@material-ui/styles';
import _get from "lodash/get";
import _remove from "lodash/remove";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const makeClasses = makeStyles((theme) => {
    return {
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        formControl: {
            width: '100%',
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1 / 2),
        },
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: theme.spacing(1 / 4),
            backgroundImage: "linear-gradient(45deg, #ffddcc, #ffddaa)"
        },
        noLabel: {
            marginTop: theme.spacing(3),
        },
        select: {
            paddingTop: `1.5px !important`,
        }
    }
});

function getStyles(name, that) {
    const enumNames = _get(that.props, 'schema.items.enumNames', []);
    return {
        fontWeight:
            enumNames.indexOf(name) === -1
                ? that.props.theme.typography.fontWeightRegular
                : that.props.theme.typography.fontWeightMedium,
    };
}

class SelectSingle extends React.Component {

    constructor(props) {
        super(props);
        this.inputId = String(Math.random())
    }

    onChange(event) {
        this.props.onChange(event.target.value);
    };

    onItemDelete(val) {
        const value = [...this.props.value];
        _remove(value, (item) => item === val);
        this.props.onChange(value)
    }

    render() {
        const enumIds = _get(this.props, 'schema.enum', []);
        const enumNames = _get(this.props, 'schema.enumNames', []);
        const {classes} = this.props;
        return <FormControl className={classes.formControl}>
            <InputLabel htmlFor={this.inputId}>{_get(this.props, 'options.title', this.props.title)}</InputLabel>
            <Select
                disabled={this.props.disabled}
                multiple={false}
                // input={<Input id={this.inputId}/>}
                classes={{
                    select: classes.select
                }}
                className={classes.select}
                value={this.props.value}
                onChange={(e) => this.onChange(e)}
            >
                {enumIds.map((id, index) => {
                    const title = enumNames[index];
                    return <MenuItem
                        key={id}
                        value={id}
                        // styleSchema={getStyles(title, this)}
                    >
                        {title}
                    </MenuItem>
                })}
            </Select>
        </FormControl>
    }

}

const SelectSingleWrapper = function (props) {
    const classes = makeClasses();
    return <SelectSingle {...props} classes={classes}/>
};

export default SelectSingleWrapper
