import React from 'react'
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import style from './style';
import {useTheme, makeStyles} from '@material-ui/styles';
import _get from 'lodash/get';
import _remove from 'lodash/remove';
import _indexOf from 'lodash/indexOf';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const makeClasses = makeStyles(style);

function getStyles(enumNames, name,  theme) {
    return {
        fontWeight:
            enumNames.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

class SelectMultiple extends React.Component {

    constructor(props) {
        super(props);
        this.inputId = String(Math.random());
    }

    onChange(event) {
        this.props.onChange(event.target.value);
    };

    onItemDelete(val) {
        const value = [...this.props.value];
        _remove(value, (item) => item === val);
        this.props.onChange(value)
    }

    render() {

        const enumIds = _get(this.props, 'schema.items.enum', []);
        const enumNames = _get(this.props, 'schema.items.enumNames', []);
        const {classes} = this.props;
        return <div className="serial-goods-filter-category">
            <FormControl className={classes.formControl}>
                <InputLabel htmlFor={this.inputId}>{_get(this.props, 'options.title', this.props.title)}</InputLabel>
                <Select
                    multiple={true}
                    input={<Input id={this.inputId}/>}
                    value={this.props.value}
                    onChange={(e) => this.onChange(e)}
                    renderValue={selected => (
                        <div className={classes.chips}>
                            {selected.map((id) => {
                                const idIndex = _indexOf(enumIds, id);
                                const title = enumNames[idIndex];
                                return <Chip
                                    key={id}
                                    label={title}
                                    onDelete={(e) => this.onItemDelete(id)}
                                    className={classes.chip}
                                />
                            })}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                    {enumIds.map((id, index) => {
                        const title = enumNames[index];
                        return <MenuItem
                            key={id}
                            value={id}
                            style={getStyles(_get(this.props, 'schema.items.enumNames', []), title, this.props.theme)}
                        >
                            <Checkbox color="primary" checked={this.props.value.indexOf(id) > -1}/>
                            <ListItemText primary={title}/>
                        </MenuItem>
                    })}
                </Select>
            </FormControl>
        </div>
    }

}

const SelectMultipleWrapper = function(props) {
    const classes = makeClasses(props);
    const theme = useTheme();
    return <SelectMultiple {...props} classes={classes} theme={theme} />
};

export default SelectMultipleWrapper
