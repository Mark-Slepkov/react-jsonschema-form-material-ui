import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        formControl: {
            width: '100%',
        },
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: theme.spacing(1 / 4),
            backgroundImage: "linear-gradient(45deg, #ffddcc, #ffddaa)"
        },
        noLabel: {
            marginTop: theme.spacing(3),
        },
    });
};
export default style;
