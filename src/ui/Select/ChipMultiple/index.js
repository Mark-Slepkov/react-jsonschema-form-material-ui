import React from 'react';
import ReactSelect from 'react-select';
import PropTypes from 'prop-types';
import _map from 'lodash/map';
import _find from 'lodash/find';
import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import clsx from 'clsx';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import ValueContainer from "./ValueContainer";
import Input from './Input'
import style from './style'
// Полная версия тут https://material-ui.com/components/autocomplete/

const makeClasses = makeStyles(style);


function MultiValue(props) {
    return <Chip
        tabIndex={-1}
        label={props.children}
        className={clsx(props.selectProps.classes.chip, {
            [props.selectProps.classes.chipFocused]: props.isFocused,
            [props.selectProps.classes.chipLoading]: props.selectProps.isLoading && !props.children,
        })}
        onDelete={props.removeProps.onClick}
        deleteIcon={<CancelIcon {...props.removeProps} />}
    />;
}

MultiValue.propTypes = {
    children: PropTypes.node,
    isFocused: PropTypes.bool,
    removeProps: PropTypes.object.isRequired,
    selectProps: PropTypes.object.isRequired,
};

class Control extends React.Component {

    static propTypes = {
        children: PropTypes.node,
        innerProps: PropTypes.object,
        innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
        selectProps: PropTypes.object.isRequired,
    };

    render() {
        const {
            children,
            innerProps,
            innerRef,
            selectProps: {classes, TextFieldProps},
        } = this.props;

        return (
            <TextField
                fullWidth
                InputProps={{
                    inputComponent: Input,
                    inputProps: {
                        className: classes.input,
                        ref: innerRef,
                        children,
                        ...innerProps,
                    },
                }}
                {...TextFieldProps}
            />
        );
    }


}


const Select = (props) => {
    const theme = useTheme();
    const classes = makeClasses();
    const options = props.options.enumOptions;

    const onChange = (value) => {
        console.log(value);
        const val = _map(value, (item) => {
            return item.value;
        });
        props.onChange(val);
    };

    const value = _map(props.value, (item) => {
        return _find(options, {value: item});
    });


    const selectStyles = {
        input: (base) => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
        menu: (base) => ({
            ...base,
            backgroundColor: theme.palette.secondary.light
        }),
        option: (base, state) => {
            return {
                ...base,
                backgroundColor: (state.isSelected || state.isFocused) ? theme.palette.secondary.main : theme.palette.secondary.light
            }
        }
    };
    // TODO: сделать генерацию inputId таким образом, чтобы id был уникальным
    return <div className={classes.root}>
        <ReactSelect
            classes={classes}
            {...props}
            styles={selectStyles}
            inputId="react-select-single"
            TextFieldProps={{
                label: props.options.title,
                InputLabelProps: {
                    htmlFor: 'react-select-single',
                    shrink: true,
                },
                placeholder: props.options.placeholder,
            }}
            value={value}
            isMulti={props.multiple}
            onChange={(value) => onChange(value)}
            onInputChange={props.onInputChange}
            options={options}
            placeholder={props.options.placeholder}
            components={{
                Control,
                // Menu,
                MultiValue,
                // NoOptionsMessage,
                // Option,
                // Placeholder,
                // SingleValue,
                ValueContainer,
            }}
        />
    </div>;
};

export default Select;
