import {emphasize} from "@material-ui/core/styles/colorManipulator";

export default (theme) => {
    let chipLoadingBackgroundImage = '';
    if (theme.palette.type === 'light') {
        chipLoadingBackgroundImage = `linear-gradient(90deg, ${theme.palette.grey[300]}, ${theme.palette.grey[400]}, ${theme.palette.grey[300]})`
    }
    else {
        chipLoadingBackgroundImage = `linear-gradient(90deg, ${theme.palette.grey[700]}, ${theme.palette.grey[600]}, ${theme.palette.grey[700]})`
    }
    return {
        root: {
            flexGrow: 1,
                height: 'auto',
        },
        input: {
            display: 'flex',
                padding: 0,
                height: 'auto',
        },
        valueContainer: {
            display: 'flex',
                flexWrap: 'wrap',
                flex: 1,
                alignItems: 'center',
                overflow: 'hidden',
        },
        chip: {
            margin: theme.spacing(0.5, 0.25),
            justifyContent: 'space-between'
        },
        chipFocused: {
            backgroundColor: emphasize(
                theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
                0.08,
            ),
        },
        chipLoading: {
            width: '100px',
            backgroundImage: chipLoadingBackgroundImage
        },
        noOptionsMessage: {
            padding: theme.spacing(1, 2),
        },
        singleValue: {
            fontSize: 16,
        },
        placeholder: {
            position: 'absolute',
                left: 2,
                bottom: 6,
                fontSize: 16,
        },
        paper: {
            position: 'absolute',
                zIndex: 1,
                marginTop: theme.spacing(1),
                left: 0,
                right: 0,
        },
        divider: {
            height: theme.spacing(2),
        },
    }
}
