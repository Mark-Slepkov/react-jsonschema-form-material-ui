import PropTypes from "prop-types";
import React from "react";

function Input({inputRef, ...props}) {
    return <div ref={inputRef} {...props} />;
}

Input.propTypes = {
    inputRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
};

export default Input
