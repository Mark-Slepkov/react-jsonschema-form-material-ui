import {createStyles} from '@material-ui/styles';
import _ from 'lodash'

const style = (theme) => {
    return createStyles({
        layout: {
            minHeight: '20px',
            width: '100%',
            textAlign: _.get(theme, 'restshop.errorBox.textAlign', 'right'),
            color: theme.palette.error.main,
            fontSize: '12px',
            lineHeight: '20px'
        },
        alignLeft: {
            textAlign: 'left'
        }
    });
};
export default style;
