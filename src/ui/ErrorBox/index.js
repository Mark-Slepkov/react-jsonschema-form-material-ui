import React from 'react'
import {makeStyles} from "@material-ui/core";
import style from './style'

const makeClasses = makeStyles(style);


class StyledErrorBox extends React.Component {
    render() {
        let children = this.props.children || [];
        let className = this.props.classes.layout;
        if (this.props.align === 'left') {
            className += ` ${this.props.classes.alignLeft}`
        }
        return (
            <div className={className}>
                {children.map((child, index)=> (<div key={index}>{child}</div>))}
            </div>
        )
    }
}

const ErrorBox = (props) => {
    const classes = makeClasses(props)
    return <StyledErrorBox {...props} classes={classes} />
}

export default ErrorBox
