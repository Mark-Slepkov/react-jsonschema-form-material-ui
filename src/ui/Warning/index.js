import React from 'react'
import IconWarning from '@material-ui/icons/Warning';
import style from './style'
import {makeStyles} from "@material-ui/core/styles";

const makeClasses = makeStyles(style)

class StyledWarning extends React.Component {
    render() {
        let visibility = this.props.visible ? 'visible' : 'hidden';
        const classes = this.props.classes;
        return <div className={classes.layout} style={{visibility}}>
            <div className={classes.iconContainer}>
                <IconWarning/>
            </div>
            <div className={classes.content}>
                {this.props.children}
            </div>
        </div>
    }
}

const Warning = (props) => {
    const classes = makeClasses(props)
    return <StyledWarning {...props} classes={classes}/>
}

export default Warning
