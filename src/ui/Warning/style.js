import {createStyles} from '@material-ui/styles';

const style = (theme) => {
    return createStyles({
        layout: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            minWidth: '100px',
            padding: '20px 0'
        },
        iconContainer: {
            color: theme.palette.warning.main,
            width: '100px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        content: {
            width: 'auto',
            color: theme.palette.warning.main,
            textAlign: 'left'
        }
    });
};
export default style;
