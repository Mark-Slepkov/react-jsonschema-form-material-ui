import React from 'react'
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";

class BooleanSwitch extends React.Component {

    onChange = (e, newValue) => {
        this.props.onChange(newValue)
    }

    render() {
        return <FormControlLabel
            control={<Switch checked={this.props.value} color={"primary"} onChange={this.onChange}/>}
            label={this.props.options.title}
            labelPlacement={'start' || this.props.options.labelPlacement}
        />
    }

}

export default BooleanSwitch;
