let path = require("path");
let webpack = require('webpack');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let PROJECT_ENV = process.env.PROJECT_ENV || 'test';
let SHOP_ID = process.env.SHOP_ID;

let config = {
    // display_modules: true,
    entry: {
        cart: ['babel-polyfill', './src/index.js'],
        //'./src/index.js',
        // tests: './src/tests',
        // ui: './src/ui',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
        filename: '[name].js?[hash]'
    },
    resolve: {
        modules: ['node_modules', 'src'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                include: [
                    /(node_modules\/restshop-ui)/,
                    /(node_modules\/@material-ui)/,
                    /(src)/,
                ],
                // exclude: /(node_modules\/*)/,
               // exclude: /(src\/ui\/ImagePicker\/)/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            sourceMap: PROJECT_ENV !== 'production',
                            compact: false,
                            presets: ["@babel/preset-react", "@babel/preset-env"],
                            plugins: [
                                "@babel/plugin-proposal-object-rest-spread",
                                "@babel/plugin-proposal-class-properties",
                                "@babel/plugin-syntax-dynamic-import",
                                //   "@babel/plugin-transform-runtime",

                            ],
                        }
                    }
                ]
            },
            {
                test: /\.(tsx|ts)$/,
                include: [
                    /(node_modules\/restshop-ui)/,
                    /(node_modules\/restshop-cart)/,
                    /(node_modules\/@material-ui)/,
                    /(src)/,
                ],
                // exclude: /(node_modules\/*)/,
                use: [
                    {
                        loader: "awesome-typescript-loader",
                        options: {allowTsInNodeModules: true}
                    }
                ]
            },
            {test: /\.svg$/, loader: 'svg-inline-loader'},
            {
                test: /\.(sass|scss|css)$/,
                // loader: 'style-loader!css-loader!sass-loader',
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: process.env.NODE_ENV === 'development',
                        },
                    },
                    'css-loader',
                    // 'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(md)$/,
                // loader: 'style-loader!css-loader!sass-loader',
                use: [
                    {loader: 'raw-loader'},
                ]
            },

            {
                test: /\.(gif|png|jpg|ttf|otf|eot|woff|woff2)$/,
                loader: 'file-loader?name=assets/[hash].[ext]'
            }
        ]
    },
    devtool: "source-map",
    plugins: [
        // new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'style.css',
            chunkFilename: 'style.css',
        }),

        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(PROJECT_ENV),
                PROJECT_ENV: JSON.stringify(PROJECT_ENV),
                SHOP_ID: JSON.stringify(SHOP_ID),
                RUNTIME: JSON.stringify('browser')
            }
        }),


        new HtmlWebpackPlugin({
            template: 'src/index.html',
            async: true,
            inject: 'body',
            chunks: ['cart']
        }),
    ],
    devServer: {
        disableHostCheck: true,
        historyApiFallback: true,
        host: '0.0.0.0',
        port: 3000,
    }
};
console.log('PROJECT_ENV: ', PROJECT_ENV);
switch (PROJECT_ENV) {
    case 'test':
        break;
    case 'development':
        break;
    case 'production':

    // config.plugins.push(
    //     new UglifyJSPlugin({
    //         cache: true,
    //         test: /\.js($|\?)/i,
    //         sourceMap: PROJECT_ENV !== 'production',
    //         uglifyOptions: {
    //             output: {
    //                 comments: false
    //             }
    //         }
    //     })
    // )
}
module.exports = config;